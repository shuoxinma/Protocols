{
    "author": "Opentrons",
    "categories": {
        "Sample Prep": [
            "Serial Dilution"
        ]
    },
    "deck-setup": "\n",
    "description": "This protocol serially dilutes stock from the stock tube rack (slot 4) to up to 15 tubes in the dilution rack (see diagram below). Volume, tube source, tube destination, and slot source/destination are all read by the robot to perform all transfer steps. 2 mix steps are also included at half tube depth to avoid pipette submersion.\nExplanation of complex parameters below:\n* csv: Import a csv file with the following format (you do not need to specify mix steps). Note that volumes should be in mL:\n\n\nP1000 Single-Channel Mount: Specify which mount (left or right) to host the P1000 single-channel pipette.\n\n",
    "internal": "469c70",
    "labware": "\nOpentrons 4-in-1 tube rack\nOpentrons 1000ul Tips\nNest 1-well Reservoir 195mL\n",
    "markdown": {
        "author": "[Opentrons](https://opentrons.com/)\n\n",
        "categories": "* Sample Prep\n\t* Serial Dilution\n\n",
        "deck-setup": "![deck layout](https://opentrons-protocol-library-website.s3.amazonaws.com/custom-README-images/469c70/Screen+Shot+2021-11-30+at+7.51.44+AM.png)\n\n---\n\n",
        "description": "This protocol serially dilutes stock from the stock tube rack (slot 4) to up to 15 tubes in the dilution rack (see diagram below). Volume, tube source, tube destination, and slot source/destination are all read by the robot to perform all transfer steps. 2 mix steps are also included at half tube depth to avoid pipette submersion.\n\nExplanation of complex parameters below:\n* `csv`: Import a csv file with the following format (you do not need to specify mix steps). Note that volumes should be in mL:\n![csv layout](https://opentrons-protocol-library-website.s3.amazonaws.com/custom-README-images/469c70/Screen+Shot+2021-11-30+at+10.36.56+AM.png)\n\n* `P1000 Single-Channel Mount`: Specify which mount (left or right) to host the P1000 single-channel pipette.\n\n\n\n\n---\n\n",
        "internal": "469c70\n",
        "labware": "* [Opentrons 4-in-1 tube rack](https://shop.opentrons.com/collections/racks-and-adapters/products/tube-rack-set-1)\n* [Opentrons 1000ul Tips](https://shop.opentrons.com/collections/opentrons-tips)\n* [Nest 1-well Reservoir 195mL](https://shop.opentrons.com/collections/reservoirs/products/nest-1-well-reservoir-195-ml)\n\n\n\n",
        "notes": "If you have any questions about this protocol, please contact the Protocol Development Team by filling out the [Troubleshooting Survey](https://protocol-troubleshooting.paperform.co/).\n\n",
        "pipettes": "* [P1000 Single-Channel Pipette](https://shop.opentrons.com/collections/ot-2-robot/products/single-channel-electronic-pipette)\n\n---\n\n",
        "process": "1. Input your protocol parameters above.\n2. Download your protocol and unzip if needed.\n3. Upload your custom labware to the [OT App](https://opentrons.com/ot-app) by navigating to `More` > `Custom Labware` > `Add Labware`, and selecting your labware files (.json extensions) if needed.\n4. Upload your protocol file (.py extension) to the [OT App](https://opentrons.com/ot-app) in the `Protocol` tab.\n5. Set up your deck according to the deck map.\n6. Calibrate your labware, tiprack and pipette using the OT App. For calibration tips, check out our [support articles](https://support.opentrons.com/en/collections/1559720-guide-for-getting-started-with-the-ot-2).\n7. Hit 'Run'.\n\n",
        "protocol-steps": "1. Stock is transferred from tube 1 of the stock rack to the first dilution rack.\n2. The stock is stepped down according to the csv.\n3. Steps 1 and 2 are repeated for all stock tubes.\n\n\n",
        "title": "Serial Dilution of Analyte Stock"
    },
    "notes": "If you have any questions about this protocol, please contact the Protocol Development Team by filling out the Troubleshooting Survey.",
    "pipettes": "\nP1000 Single-Channel Pipette\n\n",
    "process": "\nInput your protocol parameters above.\nDownload your protocol and unzip if needed.\nUpload your custom labware to the OT App by navigating to More > Custom Labware > Add Labware, and selecting your labware files (.json extensions) if needed.\nUpload your protocol file (.py extension) to the OT App in the Protocol tab.\nSet up your deck according to the deck map.\nCalibrate your labware, tiprack and pipette using the OT App. For calibration tips, check out our support articles.\nHit 'Run'.\n",
    "protocol-steps": "\nStock is transferred from tube 1 of the stock rack to the first dilution rack.\nThe stock is stepped down according to the csv.\nSteps 1 and 2 are repeated for all stock tubes.\n",
    "title": "Serial Dilution of Analyte Stock"
}